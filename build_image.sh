#!/bin/bash

mmc_is_valid()
{
    if [ "${1:-}" ]; then
        BLK_DEV=$1
    fi

    if ! [[ -a "${BLK_DEV}" ]]; then
        return 1
    fi
}

print_help()
{
    if [ ! -z ${1+x} ]; then
        echo "$1"
    fi

    echo ""
    echo "$ ./`basename $0` [option]"
    echo ""
    echo "Actions:"
    echo "-b [device]      -- Build everything and create bootable image on device"
    echo ""

    if [ ! -z ${2+x} ]; then
        echo "$2"
    fi

    exit 1
}

parse_args()
{
    while getopts ":kab:d:" opt;
    do
    case "$opt" in
        b)
            mmc_is_valid $OPTARG || print_help "Invalid MMC supplied!"
            BLK_DEV=$OPTARG
            ;;
        *)
            echo "*)"
            print_help "Invalid argument -$opt $OPTARG"
            ;;
        esac
    done
    shift $((OPTIND-1))
}

ROOTDIR="`dirname "$0"`/.."
ROOTDIR="`realpath "$ROOTDIR"`"
cd $ROOTDIR

parse_args $@

mkdir -p debos/overlays/system/boot
echo "Adding RockPi4b Support"
cp linux/arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4.dtb debos/overlays/system/boot/.

echo "Adding Linux Kernel"
cp linux/arch/arm/boot/zImage debos/overlays/system/boot/.

cd $ROOTDIR/debos
sudo chmod 0666 /dev/kvm
docker run \
    --rm \
    --interactive \
    --tty \
    --device /dev/kvm \
    --user $(id -u) \
    --workdir /recipes \
    --mount "type=bind,source=$(pwd),destination=/recipes" \
    --security-opt label=disable \
        godebos/debos \
            --scratchsize 4G \
            image.yaml \
    || exit 0
cd - &> /dev/null

if [ ! -z ${BLK_DEV+x} ]; then
    umount $BLK_DEV* &> /dev/null
    sudo bmaptool copy --bmap debos/debian-testing-arm64.img.bmap debos/debian-testing-arm64.img $BLK_DEV
    sync
fi