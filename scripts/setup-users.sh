#!/bin/sh

USER=debian

adduser --gecos "$USER" \
		--disabled-password \
		--shell /bin/zsh \
	      "$USER"
adduser "$USER" sudo

echo "$USER:$USER" | chpasswd
echo "root:root" | chpasswd
chsh -s /bin/zsh root
